Name:           pi_gadget
Version:        0.1
Release:        11%{?dist}
Summary:        Turns your raspberry pi into an USB gadget

License:        MIT
Source0:        pi_gadget
Source1:        pi_gadget.service
Source2:        pi_gadget.preset
Source3:        ssh.service

Source10:       cockpit.preset

BuildArch:      noarch
BuildRequires:  systemd-rpm-macros
Requires:       pi_resize
Requires:       avahi
Requires:       cockpit
Requires:       cockpit-podman

%description
Script and service to turn your raspberry pi into an USB gadget

%prep

%build

%install
mkdir -p %{buildroot}%{_libexecdir}
install  -m 0755 %{SOURCE0} %{buildroot}%{_libexecdir}/%{name}

mkdir -p %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{name}.service

mkdir -p %{buildroot}%{_presetdir}
install -m 0644 %{SOURCE2} %{buildroot}%{_presetdir}/50-%{name}.preset
install -m 0644 %{SOURCE10} %{buildroot}%{_presetdir}/50-cockpit.preset

mkdir -p %{buildroot}%{_sysconfdir}/modules-load.d
echo "libcomposite" > %{buildroot}%{_sysconfdir}/modules-load.d/libcomposite.conf

mkdir -p %{buildroot}%{_sysconfdir}/avahi/services
install -m 0644 %{SOURCE3} %{buildroot}%{_sysconfdir}/avahi/services/ssh.service

%post
%systemd_post pi_gadget.service
%systemd_post cockpit.service

%preun
%systemd_preun pi_gadget.service
%systemd_preun cockpit.service

%postun
%systemd_postun pi_gadget.service
%systemd_postun cockpit.service


%files
%{_sysconfdir}/modules-load.d/libcomposite.conf
%{_sysconfdir}/avahi/services/ssh.service
%{_libexecdir}/%{name}
%{_unitdir}/%{name}.service
%{_presetdir}/50-%{name}.preset
%{_presetdir}/50-cockpit.preset


%changelog
* Wed May 04 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-11
- Change the cockpit preset to enable cockpit.socket

* Wed May 04 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-10
- Get cockpit installed as a dependency and enable it via a preset

* Tue May 03 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-9
- Include a ssh.service for avahi

* Tue May 03 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-8
- Drop the pi_resize files in favor of the pi_resize package

* Tue May 03 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-7
- Tune the pi_resize script

* Tue May 03 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-6
- Add the post/preun/postun macro for pi_resize
- Improve/simplify the pi_resize script

* Tue May 03 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-5
- Fix typo in the pi_resize service file
- Fix the Source# when installing the files (doh!)

* Mon May 02 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-4
- Add the pi_resize script, service and preset

* Tue Apr 26 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-3
- Load the libcomposite kernel module at boot

* Tue Apr 26 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-2
- Fix the service name in the preset file

* Mon Apr 25 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1-1
- Initial version

